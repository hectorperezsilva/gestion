<?php
$page_title = 'Subir Archivos';
include 'inc/header.php';
?>
<!-- <link rel="stylesheet" href="upload/css/style.css">
blueimp Gallery styles -->
<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="upload/css/jquery.fileupload.css">
<link rel="stylesheet" href="upload/css/jquery.fileupload-ui.css">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="upload/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="upload/css/jquery.fileupload-ui-noscript.css"></noscript>	





<div class="container" >
	<div class="row">
		<div class="col-xs-12">	
			<section class="content-header" style="margin-bottom:15px; margin-right:35px">
			  <h1>
				<li class="fa fa-file"></li>
				Subir Archivos
			  </h1>
			  <ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-dashboard"></i>Inicio</a></li>
				<li><a href="upload.php">  Subir Archivos</a></li>
			  </ol>
			</section>
			<hr>
		
		<!-- The file upload form used as target for the file upload widget -->
			<form id="fileupload" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
				<!-- Redirect browsers with JavaScript disabled to the origin page -->
				<noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
				<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
				<div class="row fileupload-buttonbar">
					<div class="col-lg-7">
						<!-- The fileinput-button span is used to style the file input field as button -->
						
						<span class="btn btn-success fileinput-button">
							<i class="glyphicon glyphicon-plus"></i>
							<span>Agregar Archivos...</span>
							<input type="file" name="files[]" multiple>
						</span>
						<button type="submit" class="btn btn-primary start">
							<i class="glyphicon glyphicon-upload"></i>
							<span>Subir</span>
						</button>
						<button type="reset" class="btn btn-warning cancel">
							<i class="glyphicon glyphicon-ban-circle"></i>
							<span>Cancelar</span>
						</button>
						<button type="button" class="btn btn-danger delete">
							<i class="glyphicon glyphicon-trash"></i>
							<span>Eliminar</span>
						</button>
						<input type="checkbox" class="toggle" id="upload_check"><label for="upload_check">Seleccionar Todo</label>
						
						<!-- The global file processing state -->
						<span class="fileupload-process"></span>
					</div>
					<!-- The global progress state -->
					<div class="col-lg-5 fileupload-progress fade">
						<!-- The global progress bar -->
						<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
							<div class="progress-bar progress-bar-success" style="width:0%;"></div>
						</div>
						<!-- The extended global progress state -->
						<div class="progress-extended">&nbsp;</div>
					</div>
				</div>
				<!-- The table listing the files available for upload/download -->
				<table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
			</form>
			
		
		<br>
		</div>
	</div>
	
</div>	
<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>

<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Procesando...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Subir</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancelar</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
				{% if(file.name.split('.').pop() === 'pdf'){ %}
					<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="upload/img/pdf.png" width="50"></a>
                {% } %}
				{% if(file.name.split('.').pop() === 'doc' || file.name.split('.').pop() === 'dot' || file.name.split('.').pop() === 'docx' || file.name.split('.').pop() === 'docm' || file.name.split('.').pop() === 'dotx' || file.name.split('.').pop() === 'dotm' || file.name.split('.').pop() === 'docb'){ %}
					<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="upload/img/word.png" width="50"></a>
                {% } %}
				{% if(file.name.split('.').pop() === 'xls' || file.name.split('.').pop() === 'xlt' || file.name.split('.').pop() === 'xlm' || file.name.split('.').pop() === 'xlsx' || file.name.split('.').pop() === 'xlsm' || file.name.split('.').pop() === 'xltx' || file.name.split('.').pop() === 'xltm' || file.name.split('.').pop() === 'xlsb' || file.name.split('.').pop() === 'xla' || file.name.split('.').pop() === 'xlam' || file.name.split('.').pop() === 'xll' || file.name.split('.').pop() === 'xlw'){ %}
					<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="upload/img/excel.png" width="50"></a>
                {% } %}
				{% if(file.name.split('.').pop() === 'ppt' || file.name.split('.').pop() === 'pot' || file.name.split('.').pop() === 'pps' || file.name.split('.').pop() === 'pptx' || file.name.split('.').pop() === 'pptm' || file.name.split('.').pop() === 'potx' || file.name.split('.').pop() === 'potm' || file.name.split('.').pop() === 'ppam' || file.name.split('.').pop() === 'ppsx' || file.name.split('.').pop() === 'ppsm' || file.name.split('.').pop() === 'sldx' || file.name.split('.').pop() === 'sldm'){ %}
					<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="upload/img/ppt.png" width="50"></a>
                {% } %}
				{% if(file.name.split('.').pop() === 'ACCDB' || file.name.split('.').pop() === 'ACCDE' || file.name.split('.').pop() === 'ACCDT' || file.name.split('.').pop() === 'ACCDR'){ %}
					<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="upload/img/access.png" width="50"></a>
                {% } %}
				{% if(file.name.split('.').pop() === 'pub'){ %}
					<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="upload/img/publ.png" width="50"></a>
                {% } %}
				{% if(file.name.split('.').pop() === 'rar' || file.name.split('.').pop() === 'zip'){ %}
					<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="upload/img/rar.png" width="50"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Eliminar</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>

<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="upload/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- blueimp Gallery script -->
<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="upload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="upload/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="upload/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="upload/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="upload/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="upload/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="upload/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="upload/js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script src="upload/js/main.js"></script>
<?php
include 'inc/footer.php';