<?php

session_start();
if(!isset($_SESSION['admin'])){
	echo '<script>window.location="login.php"</script>';
}

require '../libraries/fpdf/fpdf.php';


$fpdf = new FPDF('P', 'mm', 'Letter');

$fpdf->SetMargins(20, 18);
$fpdf->AliasNbPages();
$fpdf->AddPage();

$fpdf->SetTextColor(0x00,0x00,0x00);
$fpdf->SetFont("Arial", "b", 9);
$fpdf->Cell(0, 5, 'Reporte de usuarios', 0, 1, 'C');

$conexion = new mysqli('localhost','root','','startup_crm');
mysqli_set_charset($conexion, "utf8");

$fpdf->Ln();

if($_GET['q'] === ''){
	
	$query = "select * from usuarios";
	$result = mysqli_query($conexion, $query);
	$total = mysqli_num_rows($result);
	
	if($total > 0){
		$fpdf->Cell(15, 5, '#', 1, 0, 'C');
		$fpdf->Cell(50, 5, 'Nombre', 1, 0, 'C');
		$fpdf->Cell(30, 5, 'Nombre de Usuario', 1, 0, 'C');
		$fpdf->Cell(50, 5, 'Email', 1, 1, 'C');
	
		while($row = mysqli_fetch_array($result)){
			
			$fpdf->Cell(15, 5, $row['id'], 1, 0, 'C');
			$fpdf->Cell(50, 5, $row['nombre'], 1, 0, 'C');
			$fpdf->Cell(30, 5, $row['username'], 1, 0, 'C');
			$fpdf->Cell(50, 5, $row['email'], 1, 1, 'C');
		}
		
		$fpdf->Output('D', 'Reporte de Usuarios.pdf');
		
	}
	else{
		
		$fpdf->Cell(0, 5, 'No se han encontrado resultados', 0, 1, 'C');
		
		$fpdf->Output('D', 'Reporte de Usuarios.pdf');
		
	}
	
	
}
else{
	
	
	
	$query = "select * from usuarios where nombre LIKE '%{$_GET['q']}%' OR username LIKE '%{$_GET['q']}%'";
	$result = mysqli_query($conexion, $query);
	$total = mysqli_num_rows($result);
	
	if($total > 0){
		
		$fpdf->Cell(15, 5, '#', 1, 0, 'C');
		$fpdf->Cell(50, 5, 'Nombre', 1, 0, 'C');
		$fpdf->Cell(30, 5, 'Nombre de Usuario', 1, 0, 'C');
		$fpdf->Cell(50, 5, 'Email', 1, 1, 'C');
	
		while($row = mysqli_fetch_array($result)){
			
			$fpdf->Cell(15, 5, $row['id'], 1, 0, 'C');
			$fpdf->Cell(50, 5, $row['nombre'], 1, 0, 'C');
			$fpdf->Cell(30, 5, $row['username'], 1, 0, 'C');
			$fpdf->Cell(50, 5, $row['email'], 1, 1, 'C');
		}
		
		$fpdf->Output('D', 'Reporte de Usuarios.pdf');
		
	}
	else{
		
		$fpdf->Cell(0, 5, 'No se han encontrado resultados', 0, 1, 'C');
		
		$fpdf->Output('D', 'Reporte de Usuarios.pdf');
		
	}
	
}