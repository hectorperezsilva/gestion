<?php
session_start();
if(!isset($_SESSION['admin'])){
	header("Location: ../login.php");
}

include '../libraries/excel/Classes/PHPExcel.php';
$objXLS = new PHPExcel();
$objSheet = $objXLS->setActiveSheetIndex(0);
	
$conexion = new mysqli('localhost','root','','startup_crm');
mysqli_set_charset($conexion, "utf8");

if($_GET['q'] === ''){

	$numero = 1;
	$query = "select * from usuarios";
	$result = mysqli_query($conexion, $query);
	$total = mysqli_num_rows($result);
	if($total > 0){
		$objSheet->setCellValue('A1', '#');
		$objSheet->setCellValue('B1', 'Nombre');
		$objSheet->setCellValue('C1', 'Nombre de Usuario');
		$objSheet->setCellValue('D1', 'Correo Electrónico');
		while($row = mysqli_fetch_array($result)){
			$numero++;
			$objSheet->setCellValue('A'.$numero, $row['id']);
			$objSheet->setCellValue('B'.$numero, $row['nombre']);
			$objSheet->setCellValue('C'.$numero, $row['username']);
			$objSheet->setCellValue('D'.$numero, $row['email']);
		}
			
		$objXLS->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
		$objXLS->getActiveSheet()->setTitle('Reporte de Usuarios');
		$objXLS->setActiveSheetIndex(0);
		$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5');
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Reporte de Usuarios.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public');
		$objWriter->save('php://output');
		//echo 'Archivo Guardado en '.(__DIR__ . "\Paises.xls");
			
			
	}
	else{
		$objSheet->setCellValue('A1', 'No se han encontrado resultados');
		$objXLS->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
		$objXLS->getActiveSheet()->setTitle('Reporte de Usuarios');
		$objXLS->setActiveSheetIndex(0);
		$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5');
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Reporte de Usuarios.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public');
		$objWriter->save('php://output');
		//echo 'Archivo Guardado en '.(__DIR__ . "\Paises.xls");
	}
	
}
else{
	
	
	$numero = 1;
	$query = "select * from usuarios where nombre LIKE '%{$_GET['q']}%' OR username LIKE '%{$_GET['q']}%'";
	$result = mysqli_query($conexion, $query);
	$total = mysqli_num_rows($result);
	if($total > 0){
		$objSheet->setCellValue('A1', '#');
		$objSheet->setCellValue('B1', 'Nombre');
		$objSheet->setCellValue('C1', 'Nombre de Usuario');
		$objSheet->setCellValue('D1', 'Correo Electrónico');
		while($row = mysqli_fetch_array($result)){
			$numero++;
			$objSheet->setCellValue('A'.$numero, $row['id']);
			$objSheet->setCellValue('B'.$numero, $row['nombre']);
			$objSheet->setCellValue('C'.$numero, $row['username']);
			$objSheet->setCellValue('D'.$numero, $row['email']);
		}
			
		$objXLS->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
		$objXLS->getActiveSheet()->setTitle('Reporte de Usuarios');
		$objXLS->setActiveSheetIndex(0);
		$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5');
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Reporte de Usuarios.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public');
		$objWriter->save('php://output');
		//echo 'Archivo Guardado en '.(__DIR__ . "\Paises.xls");
			
			
	}
	else{
		$objSheet->setCellValue('A1', 'No se han encontrado resultados');
		$objXLS->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
		$objXLS->getActiveSheet()->setTitle('Reporte de Usuarios');
		$objXLS->setActiveSheetIndex(0);
		$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5');
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Reporte de Usuarios.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public');
		$objWriter->save('php://output');
		//echo 'Archivo Guardado en '.(__DIR__ . "\Paises.xls");
	}
	
	
}
	
	

