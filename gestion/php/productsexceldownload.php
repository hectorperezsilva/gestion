<?php
session_start();
if(!isset($_SESSION['admin'])){
	header("Location: ../login.php");
}

include '../libraries/excel/Classes/PHPExcel.php';
$objXLS = new PHPExcel();
$objSheet = $objXLS->setActiveSheetIndex(0);
	
$conexion = new mysqli('localhost','root','','startup_crm');
mysqli_set_charset($conexion, "utf8");

if($_GET['q'] === ''){
	
	$numero = 1;
	$query = "select * from productos";
	$result = mysqli_query($conexion, $query);
	$total = mysqli_num_rows($result);
	if($total > 0){
		$objSheet->setCellValue('A1', '#');
		$objSheet->setCellValue('B1', 'Nombre');
		$objSheet->setCellValue('C1', 'Codigo');
		$objSheet->setCellValue('D1', 'Precio');
		$objSheet->setCellValue('E1', 'Stock');
		while($row = mysqli_fetch_array($result)){
			$numero++;
			$objSheet->setCellValue('A'.$numero, $row['id_producto']);
			$objSheet->setCellValue('B'.$numero, $row['nombre_producto']);
			$objSheet->setCellValue('C'.$numero, $row['codigo_producto']);
			$objSheet->setCellValue('D'.$numero, $row['precio_producto']);
			$objSheet->setCellValue('E'.$numero, $row['stock_producto']);
		}
			
		$objXLS->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
		$objXLS->getActiveSheet()->setTitle('Reporte de Productos');
		$objXLS->setActiveSheetIndex(0);
		$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5');
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Reporte de Productos.xls"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header ('Cache-Control: cache, must-revalidate'); 
		header ('Pragma: public');
		$objWriter->save('php://output');
			
			
	}
	else{
		$objSheet->setCellValue('A1', 'No se han encontrado resultados');
		$objXLS->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
		$objXLS->getActiveSheet()->setTitle('Reporte de Productos');
		$objXLS->setActiveSheetIndex(0);
		$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5');
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Reporte de Productos.xls"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header ('Cache-Control: cache, must-revalidate');
		header ('Pragma: public');
		$objWriter->save('php://output');
	}
	
}
else{
	
	$numero = 1;
	$query = "select * from productos where nombre_producto LIKE '%{$_GET['q']}%'";
	$result = mysqli_query($conexion, $query);
	$total = mysqli_num_rows($result);
	if($total > 0){
		$objSheet->setCellValue('A1', '#');
		$objSheet->setCellValue('B1', 'Nombre');
		$objSheet->setCellValue('C1', 'Codigo');
		$objSheet->setCellValue('D1', 'Precio');
		$objSheet->setCellValue('E1', 'Stock');
		while($row = mysqli_fetch_array($result)){
			$numero++;
			$objSheet->setCellValue('A'.$numero, $row['id_producto']);
			$objSheet->setCellValue('B'.$numero, $row['nombre_producto']);
			$objSheet->setCellValue('C'.$numero, $row['codigo_producto']);
			$objSheet->setCellValue('D'.$numero, $row['precio_producto']);
			$objSheet->setCellValue('E'.$numero, $row['stock_producto']);
		}
			
		$objXLS->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
		$objXLS->getActiveSheet()->setTitle('Reporte de Productos');
		$objXLS->setActiveSheetIndex(0);
		$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5');
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Reporte de Productos.xls"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); 
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); 
		header ('Cache-Control: cache, must-revalidate'); 
		header ('Pragma: public');
		$objWriter->save('php://output');
			
			
	}
	else{
		$objSheet->setCellValue('A1', 'No se han encontrado resultados');
		$objXLS->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
		$objXLS->getActiveSheet()->setTitle('Reporte de Productos');
		$objXLS->setActiveSheetIndex(0);
		$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5');
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Reporte de Productos.xls"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); 
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); 
		header ('Cache-Control: cache, must-revalidate');
		header ('Pragma: public');
		$objWriter->save('php://output');
	}
	
	
}
	
	
