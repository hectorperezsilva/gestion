<?php

if($_POST){
	
	if(empty($_POST['user'])){
		echo '<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong>Debes ingresar un nombre de usuario</div>';
	}
	elseif(empty($_POST['pass'])){
		echo '<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong>Debes ingresar una contraseña</div>';
	}
	elseif(preg_match('/\s/', $_POST['user'])){
		echo '<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong>El nombre de usuario no puede tener espacios en blanco</div>';
	}
	elseif(preg_match('/\s/', $_POST['pass'])){
		echo '<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong>La contraseña no puede tener espacios en blanco</div>';
	}
	else{
		
		include '../config/conexion.php';
		$conexion = connect();
		$user = htmlspecialchars($_POST['user']);
		$pass = sha1($_POST['pass']);
		$query = "Select * from usuarios where username = '{$user}' and password = '{$pass}'";
		$result = mysqli_query($conexion, $query);
		$total = mysqli_num_rows($result);
		
		if($total > 0){
			
			$row = mysqli_fetch_array($result);
			session_start();
			$_SESSION['admin'] = $row['username'];
			echo '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert">&times;</button>Bienvenido '.$_SESSION['admin'].'</div>';
			echo '<meta http-equiv="refresh" content="0; url=http://localhost/gestion/" />';
			
		}
		else{
			echo '<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong>Claves de Acceso Incorrectas</div>';
		}
		
	}
}
else{
	echo '<meta http-equiv="refresh" content="0; url=http://localhost/gestion/" />';
}

