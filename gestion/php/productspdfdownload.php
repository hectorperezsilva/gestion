<?php

session_start();
if(!isset($_SESSION['admin'])){
	echo '<script>window.location="login.php"</script>';
}
require '../libraries/fpdf/fpdf.php';
$fpdf = new FPDF('P', 'mm', 'Letter');

$fpdf->SetMargins(20, 18);
$fpdf->AliasNbPages();
$fpdf->AddPage();

$fpdf->SetTextColor(0x00,0x00,0x00);
$fpdf->SetFont("Arial", "b", 9);
$fpdf->Cell(0, 5, 'Reporte de Productos', 0, 1, 'C');

$conexion = new mysqli('localhost','root','','startup_crm');
mysqli_set_charset($conexion, "utf8");

$fpdf->Ln();
	
if($_GET['q'] === ''){
	
	
	$query = "select * from productos";
	$result = mysqli_query($conexion, $query);
	$total = mysqli_num_rows($result);
	
	if($total > 0){
		
		$fpdf->Cell(20, 5, '#', 1, 0, 'C');
		$fpdf->Cell(80, 5, 'Nombre', 1, 0, 'C');
		$fpdf->Cell(30, 5, 'Codigo', 1, 0, 'C');
		$fpdf->Cell(30, 5, 'Precio', 1, 0, 'C');
		$fpdf->Cell(20, 5, 'Stock', 1, 1, 'C');
	
		while($row = mysqli_fetch_array($result)){
			
			$fpdf->Cell(20, 5, $row['id_producto'], 1, 0, 'C');
			$fpdf->Cell(80, 5, $row['nombre_producto'], 1, 0, 'C');
			$fpdf->Cell(30, 5, $row['codigo_producto'], 1, 0, 'C');
			$fpdf->Cell(30, 5, $row['precio_producto'], 1, 0, 'C');
			$fpdf->Cell(20, 5, $row['stock_producto'], 1, 1, 'C');
		}
		
		$fpdf->Output('D', 'Reporte de Productos.pdf');
		
	}
	else{
		
		$fpdf->Cell(0, 5, 'No se han encontrado resultados', 0, 1, 'C');
		
		$fpdf->Output('D', 'Reporte de Productos.pdf');
		
	}
	
	
}
else{
	
	$query = "select * from productos where nombre_producto LIKE '%{$_GET['q']}%'";
	$result = mysqli_query($conexion, $query);
	$total = mysqli_num_rows($result);
	
	if($total > 0){
		
		$fpdf->Cell(20, 5, '#', 1, 0, 'C');
		$fpdf->Cell(80, 5, 'Nombre', 1, 0, 'C');
		$fpdf->Cell(30, 5, 'Codigo', 1, 0, 'C');
		$fpdf->Cell(30, 5, 'Precio', 1, 0, 'C');
		$fpdf->Cell(20, 5, 'Stock', 1, 1, 'C');
	
		while($row = mysqli_fetch_array($result)){
			
			$fpdf->Cell(20, 5, $row['id_producto'], 1, 0, 'C');
			$fpdf->Cell(80, 5, $row['nombre_producto'], 1, 0, 'C');
			$fpdf->Cell(30, 5, $row['codigo_producto'], 1, 0, 'C');
			$fpdf->Cell(30, 5, $row['precio_producto'], 1, 0, 'C');
			$fpdf->Cell(20, 5, $row['stock_producto'], 1, 1, 'C');
		}
		
		$fpdf->Output('D', 'Reporte de Productos.pdf');
		
	}
	else{
		
		$fpdf->Cell(0, 5, 'No se han encontrado resultados', 0, 1, 'C');
		
		$fpdf->Output('D', 'Reporte de Productos.pdf');
		
	}
	
}