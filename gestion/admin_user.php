<?php
$page_title = "Administración de Usuarios";
include 'inc/header.php';
?>

	<section class="content-header" style="margin-bottom:15px;">
      <h1>
        <li class="fa fa-user"></li>
		Administración de Usuarios
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i>Inicio</a></li>
        <li><a href="admin_user.php">  Administración de Usuarios</a></li>
      </ol>
    </section>

    <!-- Main content -->
	<div class="row">
		<div class="col-xs-12">
			<div class="col-xs-12">
				<div class="col-md-12" style="margin-top:5px; margin-bottom:5px" >
					<button type='button' class="btn btn-primary" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus" ></span> Nuevo Usuario</button>
				</div>
			</div>
		</div>
	</div>
    <div class="row">
		<div class="col-xs-12">
			<div class="col-xs-12">
				<div class="col-md-3" style="margin-top:5px; margin-bottom:5px">
					<input type="text" id="q" name="q" class="form-control" placeholder="Buscar Usuario" onkeyup="buscar_usuario(this.value)">
				</div>
				<!--<div class="col-md-2" style="margin-top:5px; margin-bottom:5px">
					<p style="margin-top:5px; margin-left:10px;">Elementos por página</p>
				</div>
				<div class="col-md-2" style="margin-top:5px; margin-bottom:5px" >
					<select id="perpage" name="perpage" class="input-sm" >
						<option value="5">5</option>
						<option value="10">10</option>
						<option value="20">20</option>
						<option value="30">30</option>
						<option value="50">50</option>
						<option value="100">100</option>
					</select>
				</div>-->
				<div class="col-md-2 col-md-offset-4" style="margin-top:5px; margin-bottom:5px">
					<button onclick="exceldownload()" title="Descargar Reporte en Excel" class="btn btn-success">Descargar   &nbsp;  <i class="fa fa-file-excel-o"></i></button>
				</div>
				<div class="col-md-2" style="margin-top:5px; margin-bottom:5px">
					<button onclick="pdfdownload()" title="Descargar Reporte en PDF" class="btn btn-danger">Descargar   &nbsp;  <i class="fa fa-file-pdf-o"></i></button>
				</div>
				
			</div>
			
			
			<br><br>
			<?php include 'modals/registro_usuarios.php'; ?>
			<?php include "modals/editar_usuarios.php"; ?>
			<div class="col-xs-12">
				<div id="results2">
				</div>
			</div>
		</div>
    </div>

	<script>
		$(document).ready(function(){
			
			load(1, $("#q").val());
		});

		function load(page, q){
			
			var q= $("#q").val();
			
			$("#loader").fadeIn('slow');
			$.ajax({
				url:'./ajax/buscar_usuarios.php?action=ajax&page='+page+'&q='+q,
				 beforeSend: function(objeto){
				 $('#loader').html('<img src="./img/ajax-loader.gif"> Cargando...');
			  },
				success:function(data){
					$("#results2").html(data).fadeIn('slow');
					$('#loader').html('');
					
				}
			})
		}

	
		
			function eliminar (id){
				var q= $("#q").val();
				swal({
					
					title: "Estás seguro?",
					text: "Deseas eliminar este usuario???!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: '#DD6B55',
					confirmButtonText: 'Sí, estoy seguro!',
					cancelButtonText: "No, cancelar!",
					closeOnConfirm: false,
					closeOnCancel: false
					
				}).then(function () {
				    $.ajax({
						type: "GET",
						url: "./ajax/buscar_usuarios.php",
						data: "id="+id,"q":q,
						 beforeSend: function(objeto){
							$("#results2").html("Mensaje: Cargando...");
						  },
						success: function(datos){
						$(results2).html(datos);
						load(1);
						}
					});
					  //swal(
						//'Deleted!',
						//'Your file has been deleted.',
						//'success'
					  //)
				})
				
			}
			$( "#guardar_usuario" ).submit(function( event ) {
			$('#guardar_datos').attr("disabled", true);
  
			 var parametros = $(this).serialize();
				 $.ajax({
						type: "POST",
						url: "ajax/nuevo_usuario.php",
						data: parametros,
						 beforeSend: function(objeto){
							$("#resultados_ajax").html("Mensaje: Cargando...");
						  },
						success: function(datos){
						$("#resultados_ajax").html(datos);
						$('#guardar_datos').attr("disabled", false);
						
						load(1, $("#q").val());
					  }
				});
			  event.preventDefault();
			})

				$( "#editar_usuario" ).submit(function( event ) {
				  $('#actualizar_datos2').attr("disabled", true);
				  
				 var parametros = $(this).serialize();
					 $.ajax({
							type: "POST",
							url: "ajax/editar_usuario.php",
							data: parametros,
							 beforeSend: function(objeto){
								$("#resultados_ajax2").html("Mensaje: Cargando...");
							  },
							success: function(datos){
							$("#resultados_ajax2").html(datos);
							$('#actualizar_datos2').attr("disabled", false);
							load(1);
						  }
					});
				  event.preventDefault();
				})



			function obtener_datos(id){
				var username = $("#nombre"+id).val();
				var user_name = $("#username"+id).val();
				var user_email = $("#user_email"+id).val();
				
				$("#mod_id").val(id);
				$("#name2").val(username);
				$("#user_name2").val(user_name);
				$("#user_email2").val(user_email);
				
			}
			
			function buscar_usuario(string){
				load(1, string)
			}
			
			function pdfdownload(){
				var q = $('#q').val();
				window.location='php/userpdfdownload.php?q='+q;
			}
			function exceldownload(){
				var q = $('#q').val();
				window.location='php/userexceldownload.php?q='+q;
			}
</script>
<?php
include 'inc/footer.php';
?>
