<?php

include 'classes/PHPExcel.php';
$objXLS = new PHPExcel();
$objSheet = $objXLS->setActiveSheetIndex(0);


$conexion = new PDO("mysql:host=localhost;dbname=user;charset=utf8;", "root", "");

$objSheet->setCellValue('A1', 'ID');
$objSheet->setCellValue('B1', 'Codigo ISO');
$objSheet->setCellValue('C1', 'Nombre');

$number = 1;
$query = "select * from paises";
if($stmt = $conexion->prepare($query)){
	$stmt->execute();
	$total = $stmt->rowCount();
	if($total>0){
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$number++;
			$objSheet->setCellValue('A'.$number, $row['id']);
			$objSheet->setCellValue('B'.$number, $row['iso']);
			$objSheet->setCellValue('C'.$number, $row['nombre']);
		}
		
		$objXLS->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
		$objXLS->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
		$objXLS->getActiveSheet()->setTitle('Paises');
		$objXLS->setActiveSheetIndex(0);
		$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Paises.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public');
		$objWriter->save('php://output');
		//echo 'Archivo Guardado en '.(__DIR__ . "\Paises.xls");
		
		
	}
	else{
		echo 'there aren\'t results';
	}
}
else{
	echo 'bad';
}


