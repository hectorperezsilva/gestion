-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-09-2018 a las 21:30:11
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `startup_crm`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_producto` int(11) NOT NULL,
  `codigo_producto` char(20) NOT NULL,
  `nombre_producto` char(100) NOT NULL,
  `modelo_producto` varchar(30) NOT NULL,
  `stock_producto` int(255) NOT NULL,
  `id_departamento_producto` int(11) NOT NULL,
  `status_producto` tinyint(4) NOT NULL,
  `unidad_medida_producto` char(20) NOT NULL,
  `peso_producto` char(20) NOT NULL,
  `date_added` datetime NOT NULL,
  `precio_producto` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `codigo_producto`, `nombre_producto`, `modelo_producto`, `stock_producto`, `id_departamento_producto`, `status_producto`, `unidad_medida_producto`, `peso_producto`, `date_added`, `precio_producto`) VALUES
(1, 'MG01', 'Mouse inalambrico', 'MG01', 200, 3, 1, 'Cada una', 'sin peso', '2013-03-25 20:35:15', 10000),
(2, 'TCL01', 'Teclado multimedia', 'TCL01', 200, 3, 1, '', '', '2013-03-25 20:35:15', 15000),
(3, 'ILK059', 'Nuevo mini 2.4g micro inalambrico de teclado', 'ILK059', 200, 3, 1, '', '', '2013-03-25 20:35:15', 25000),
(4, '4520', 'TECLADO para HP 4520', 'MS250', 200, 3, 1, '', '', '2013-03-25 20:35:15', 40000),
(5, 'A081', 'Altavoz de la computadora', 'A081', 200, 3, 1, '', '', '2013-03-25 20:35:15', 18000),
(64, 'MSD01', '2GB Tarjeta Micro SD', 'MSD01', 200, 2, 1, '', '', '2013-03-25 20:35:15', 5000),
(65, 'AUR01', 'Adaptador usb para radio cd', 'AUR01', 200, 2, 1, '', '', '2013-03-25 20:35:15', 5000),
(66, 'SA-205', '2.0 de canal de sonido multimedia sa-205', 'SA-205', 200, 2, 1, '', '', '2013-03-25 20:35:15', 13000),
(67, 'KB-1830', 'Teclado multimedia de alta calidad', 'KB-1830', 200, 2, 1, '', '', '2013-03-25 20:35:15', 12000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(20) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  `email` varchar(30) NOT NULL,
  `fecha` datetime NOT NULL,
  `estado` smallint(11) NOT NULL,
  `codigo_activacion` varchar(60) NOT NULL,
  `codigo_recuperacion` varchar(60) DEFAULT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `username`, `password`, `email`, `fecha`, `estado`, `codigo_activacion`, `codigo_recuperacion`, `foto`) VALUES
(19, 'usuario', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'user@de.pruebas', '2016-07-27 00:50:04', 1, 'f56447182a0fa8ed42ae9f7817056a791b5b9299', NULL, 'user2-160x160.jpg'),
(20, 'Martín Concha', 'saito231', '1c6027c0667c7646b964476b61960e0cf7b559ee', 'martin@concha.alfaro', '2018-09-14 21:27:57', 1, '1c6027c0667c7646b964476b61960e0cf7b559ee', '1c6027c0667c7646b964476b61960e0cf7b559ee', 'user2-160x160.jpg'),
(21, 'Javier Molina', 'Jaslam6', '5f9360249013fabe0d88d56c4a97d2f620ba8886', 'javier@molina.molina', '2018-09-14 21:28:40', 1, '5f9360249013fabe0d88d56c4a97d2f620ba8886', '5f9360249013fabe0d88d56c4a97d2f620ba8886', 'user2-160x160.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`),
  ADD UNIQUE KEY `codigo_producto` (`codigo_producto`),
  ADD KEY `id_departamento_producto` (`id_departamento_producto`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4303;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
