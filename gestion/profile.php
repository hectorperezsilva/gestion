<?php

$page_title = "Mi Perfil";
include 'inc/header.php';
?>
	<section class="content-header" >
      <h1>
        <li class="fa fa-user"></li>
		Perfil
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i>Inicio</a></li>
        <li><a href="profile.php">  Perfil</a></li>
      </ol>
    </section>
	<section class="content">
	 <div class="row">
        <div class="col-md-10 col-md-offset-1">

          <!-- Profile Image -->
         
              <img class="profile-user-img img-responsive img-circle" src="dist/img/<?php echo $row['foto']; ?>" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo $row['username']; ?></h3>

              <p class="text-muted text-center"><?php echo $row['email']; ?></p>
				<hr>
					<div class="col-md-4 col-md-offset-2 col-xs-6">
						<b>Nombre:</b> 
					</div>		
					<div class="col-md-4 col-xs-6">
						<p class="pull-right"><?php echo $row['nombre'];?></p>
					</div>	
					<div class="col-md-4 col-md-offset-2 col-xs-6">
						<b>Usuario:</b> 
					</div>		
					<div class="col-md-4 col-xs-6">
						<p class="pull-right"><?php echo $row['username']; ?></p>
					</div>	
					<div class="col-md-4 col-md-offset-2 col-xs-6">
						<b>Email:</b> 
					</div>		
					<div class="col-md-4 col-xs-6">
						<p class="pull-right"><?php echo $row['email']; ?></p>
					</div>
					
					<div class="col-md-4 col-md-offset-2 col-xs-6">
						<b>Status:</b> 
					</div>		
					<div class="col-md-4 col-xs-6">
						<p class="pull-right"><?php 
							
							if($row['estado'] == 1){
								echo 'Usuario Activo';
							}
							else{
								echo 'Usuario Suspendido';
							}

						?></p>
					</div>	
					<hr>
					<div class="col-md-4 col-md-offset-2">
						<button disabled class="btn btn-primary">Editar Perfil</button>
					</div>
				
              
    
        </div>
	 </div>
	</section>
<?php
include 'inc/footer.php';