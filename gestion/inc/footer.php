</div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    
    <strong>&copy; 2018 Business One IT Solutions.</strong> Todos los derechos reservados
  </footer>

  <!-- Control Sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
	<script>
		$('#logout').on('click', function(){
			swal({
					
					title: "Cerrar Sesión??",
					text: "Estas seguro???!",
					type: "question",
					showCancelButton: true,
					confirmButtonColor: '#DD6B55',
					confirmButtonText: 'Sí',
					cancelButtonText: "No",
					closeOnConfirm: false,
					closeOnCancel: false
					
				}).then(function () {
				    window.location="php/logout.php"
					  //swal(
						//'Deleted!',
						//'Your file has been deleted.',
						//'success'
					  //)
				})
		})
	</script>

</body>
</html>
